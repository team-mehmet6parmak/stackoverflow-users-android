package com.mehmet6parmak.sousers.di;

import com.mehmet6parmak.sousers.userdetail.UserDetailActivity;
import com.mehmet6parmak.sousers.users.UsersActivity;

import dagger.Subcomponent;

/**
 *
 * A component providing narrower scope {@link ActivityScope} than {@link ApplicationComponent}
 *
 * Created on 6/9/2018.
 */
@ActivityScope
@Subcomponent
public interface ActivitiesComponent {

    void inject(UsersActivity activity);
    void inject(UserDetailActivity activity);
}
