package com.mehmet6parmak.sousers.di.modules;


import com.mehmet6parmak.sousers.SOApplication;
import com.mehmet6parmak.sousers.common.ApplicationSchedulers;
import com.mehmet6parmak.sousers.common.SchedulerProvider;
import com.mehmet6parmak.sousers.data.UserStatusService;
import com.mehmet6parmak.sousers.data.response.LocalUserStatusService;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 6/8/2018.
 */
@Module
public class ApplicationModule {

    private final SOApplication application;

    public ApplicationModule(SOApplication app) {
        this.application = app;
    }

    @Provides
    public SchedulerProvider provideScheduler() {
        return new ApplicationSchedulers();
    }

    @Provides
    public UserStatusService provideLocalUserStatusService() {
        return new LocalUserStatusService();
    }
}
