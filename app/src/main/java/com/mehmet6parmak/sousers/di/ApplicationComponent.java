package com.mehmet6parmak.sousers.di;

import com.mehmet6parmak.sousers.di.modules.ApiModule;
import com.mehmet6parmak.sousers.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created on 6/8/2018.
 */
@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {
    ActivitiesComponent newActivityComponent();
}
