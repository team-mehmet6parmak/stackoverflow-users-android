package com.mehmet6parmak.sousers.userdetail;

import com.mehmet6parmak.sousers.common.BasePresenterImpl;
import com.mehmet6parmak.sousers.common.SchedulerProvider;
import com.mehmet6parmak.sousers.data.UserRepository;
import com.mehmet6parmak.sousers.model.User;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created on 6/10/2018.
 */
public class UserDetailPresenter extends BasePresenterImpl<UserDetailContract.View> implements UserDetailContract.Presenter {

    private final UserRepository repository;

    @Inject
    public UserDetailPresenter(UserRepository repository, SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
        this.repository = repository;
    }

    @Override
    public void loadUserDetail(String userId) {
        repository.getUserDetails(userId)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                        onLoading();
                    }

                    @Override
                    public void onNext(User user) {
                        onUserDetailLoaded(user);
                    }

                    @Override
                    public void onError(Throwable e) {
                        onErrorLoadingUserDetail();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void followUser(User user) {
        repository.followUser(user).subscribe(new UserUpdateObserver());
    }

    @Override
    public void blockUser(User user) {
        repository.blockUser(user).subscribe(new UserUpdateObserver());
    }

    @Override
    public void unFollowUser(User user) {
        repository.unFollowUser(user).subscribe(new UserUpdateObserver());
    }

    @Override
    public void unBlockUser(User user) {
        repository.unblockUser(user).subscribe(new UserUpdateObserver());
    }

    private void onLoading() {
        if (view != null) {
            view.showLoading(true);
        }
    }

    protected void onUserDetailLoaded(User user) {
        if (view != null && view.isAlive()) {
            view.onUserDetailLoaded(user);
            view.showLoading(false);
        }
    }

    protected void onErrorLoadingUserDetail() {
        if (view != null && view.isAlive()) {
            view.showLoading(false);
            view.onError();
        }
    }

    class UserUpdateObserver implements Observer<User> {
        @Override
        public void onSubscribe(Disposable d) {
        }

        @Override
        public void onNext(User user) {
            onUserDetailLoaded(user);
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onComplete() {
        }
    }
}
