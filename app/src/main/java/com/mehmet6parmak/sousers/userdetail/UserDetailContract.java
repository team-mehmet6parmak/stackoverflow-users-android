package com.mehmet6parmak.sousers.userdetail;

import com.mehmet6parmak.sousers.common.BasePresenter;
import com.mehmet6parmak.sousers.common.BaseView;
import com.mehmet6parmak.sousers.model.User;

/**
 * Created on 6/10/2018.
 */
public class UserDetailContract {

    interface View extends BaseView {
        void onUserDetailLoaded(User user);
        void onError();
        String getUserId();
    }

    interface Presenter extends BasePresenter<View> {
        void loadUserDetail(String userId);

        void followUser(User user);
        void blockUser(User user);
        void unFollowUser(User user);
        void unBlockUser(User user);
    }
}
