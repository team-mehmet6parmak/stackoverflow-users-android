package com.mehmet6parmak.sousers.userdetail;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mehmet6parmak.sousers.R;
import com.mehmet6parmak.sousers.common.BaseActivity;
import com.mehmet6parmak.sousers.databinding.ActivityUserDetailBinding;
import com.mehmet6parmak.sousers.model.User;
import com.mehmet6parmak.sousers.utilities.DateFormatUtility;

import javax.inject.Inject;

public class UserDetailActivity extends BaseActivity implements UserDetailContract.View {

    private static final String ARG_USER_ID = "user-id";

    public static void startWithUser(Activity activity, User user) {
        Intent intent = new Intent(activity, UserDetailActivity.class);
        intent.putExtra(ARG_USER_ID, user.getUserId());

        activity.startActivity(intent);
    }

    ActivityUserDetailBinding binding;

    @Inject
    UserDetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        component.inject(this);

        presenter.setView(this);
        presenter.loadUserDetail(getUserId());
    }

    @Override
    public void onUserDetailLoaded(User user) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_profile_placeholder)
                .error(R.drawable.ic_profile_placeholder);

        Glide.with(this)
                .setDefaultRequestOptions(requestOptions)
                .load(user.getProfileImage())
                .into(binding.imgProfile);

        setText(binding.txtName, user.getName());
        setText(binding.txtReputation, getString(R.string.user_detail_reputation, String.valueOf(user.getReputation())));
        setText(binding.txtMetadata, getString(R.string.user_detail_membership, DateFormatUtility.formatMembershipDate(user.getCreationDate())));
        setText(binding.txtLocation, user.getLocation());
        setText(binding.txtId, getString(R.string.user_detail_id, user.getUserId()));

        binding.txtSocialStatus.setVisibility(user.isFollowed() ? View.VISIBLE : View.GONE);
        binding.btnFollowUnfollow.setText(user.isFollowed() ? R.string.users_unfollow : R.string.users_follow);
        binding.btnBlockUnblock.setText(user.isBlocked() ? R.string.users_unblock : R.string.users_block);

        binding.btnFollowUnfollow.setOnClickListener(v -> {
            if (user.isFollowed()) {
                presenter.unFollowUser(user);
            } else {
                presenter.followUser(user);
            }
        });

        binding.btnBlockUnblock.setOnClickListener(v -> {
            if (user.isBlocked()) {
                presenter.unBlockUser(user);
            } else {
                presenter.blockUser(user);
            }
        });
    }

    private void setText(TextView txt, String text) {
        if (TextUtils.isEmpty(text)) {
            txt.setVisibility(View.GONE);
        } else {
            txt.setVisibility(View.VISIBLE);
            txt.setText(text);
        }
    }

    @Override
    public void showLoading(boolean visible) {
        if (visible) {
            showLoading();
        }
    }

    @Override
    public void onError() {
        //This would only happen if the process get killed by the OS and repository data has been cleared.
        //For the sake of simplicity I've not implemented restoration of presenter state if the app was killed.
        finish();
    }

    @Override
    public String getUserId() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            return bundle.getString(ARG_USER_ID, "");
        }
        return "";
    }

    @Override
    public boolean isAlive() {
        return isActivityAlive();
    }

    @Override
    protected void onDestroy() {
        presenter.removeView();
        super.onDestroy();
    }
}
