package com.mehmet6parmak.sousers.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created on 6/10/2018.
 */
public class Followed extends RealmObject {

    @PrimaryKey
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Followed() {
    }

    public Followed(String userId) {
        this.userId = userId;
    }

    public static class Contract {
        public static final String COLUMN_USER_ID = "userId";
    }
}
