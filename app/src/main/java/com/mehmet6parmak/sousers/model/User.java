package com.mehmet6parmak.sousers.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 6/8/2018.
 */
public class User {

    @NonNull
    @SerializedName("user_id")
    private String userId;

    @NonNull
    @SerializedName("display_name")
    private String name;

    @NonNull
    @SerializedName("reputation")
    private Long reputation;

    @NonNull
    @SerializedName("profile_image")
    private String profileImage;

    @SerializedName("location")
    private String location;

    @NonNull
    @SerializedName("creation_date")
    private Long creationDate;

    private boolean followed;
    private boolean blocked;

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public Long getReputation() {
        return reputation;
    }

    public void setReputation(@NonNull Long reputation) {
        this.reputation = reputation;
    }

    @NonNull
    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(@NonNull String profileImage) {
        this.profileImage = profileImage;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @NonNull
    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(@NonNull Long creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
