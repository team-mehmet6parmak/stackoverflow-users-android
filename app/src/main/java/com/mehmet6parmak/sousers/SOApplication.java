package com.mehmet6parmak.sousers;

import android.app.Application;

import com.mehmet6parmak.sousers.di.ApplicationComponent;
import com.mehmet6parmak.sousers.di.modules.ApplicationModule;
import com.mehmet6parmak.sousers.di.DaggerApplicationComponent;

import io.realm.Realm;


/**
 * Created on 6/8/2018.
 */
public class SOApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        Realm.init(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
