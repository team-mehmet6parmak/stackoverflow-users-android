package com.mehmet6parmak.sousers.users;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mehmet6parmak.sousers.R;
import com.mehmet6parmak.sousers.databinding.ListItemUserBinding;
import com.mehmet6parmak.sousers.model.User;

/**
 * Created on 6/9/2018.
 */
public class UserViewHolder extends RecyclerView.ViewHolder implements UsersContract.ItemView {

    private final ListItemUserBinding binding;
    private final UsersPresenter presenter;

    public UserViewHolder(ListItemUserBinding binding, UsersPresenter presenter) {
        super(binding.getRoot());
        this.binding = binding;
        this.presenter = presenter;
    }

    @Override
    public void bindUser(User user, int position) {
        binding.txtName.setText(user.getName());
        binding.txtReputation.setText(String.valueOf(user.getReputation()));

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_profile_placeholder)
                .error(R.drawable.ic_profile_placeholder);

        Glide.with(binding.imgProfile)
                .setDefaultRequestOptions(requestOptions)
                .load(user.getProfileImage())
                .into(binding.imgProfile);

        if (user.isBlocked()) {
            binding.btnBlockUnblock.setText(R.string.users_unblock);
            binding.btnBlockUnblock.setOnClickListener(v -> presenter.unBlockUser(user, position));

            binding.getRoot().setTag(null);
            binding.getRoot().setOnClickListener(null);

            binding.layoutUserInfo.setAlpha(0.4f);
        } else {
            binding.btnBlockUnblock.setText(R.string.users_block);
            binding.btnBlockUnblock.setOnClickListener(v -> presenter.blockUser(user, position));

            binding.getRoot().setTag(user);
            binding.getRoot().setOnClickListener(v -> presenter.showUserDetail((User) v.getTag()));

            binding.layoutUserInfo.setAlpha(1.0f);
        }

        if (user.isFollowed()) {
            binding.txtSocialStatus.setVisibility(View.VISIBLE);
            binding.btnFollowUnfollow.setText(R.string.users_unfollow);
            binding.btnFollowUnfollow.setOnClickListener(v -> presenter.unFollowUser(user, position));
        } else {
            binding.txtSocialStatus.setVisibility(View.GONE);
            binding.btnFollowUnfollow.setText(R.string.users_follow);
            binding.btnFollowUnfollow.setOnClickListener(v -> presenter.followUser(user, position));
        }
    }
}
