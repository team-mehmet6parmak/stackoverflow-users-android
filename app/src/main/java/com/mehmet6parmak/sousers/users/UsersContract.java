package com.mehmet6parmak.sousers.users;


import com.mehmet6parmak.sousers.common.BasePresenter;
import com.mehmet6parmak.sousers.common.BaseView;
import com.mehmet6parmak.sousers.model.User;

/**
 * Created on 6/8/2018.
 */
public class UsersContract {

    public interface View extends BaseView {
        void onUsersLoaded();

        void showErrorLayout();

        void onUserDetailRequested(User user);

        void onUserUpdated(User user, int userListIndex);
    }

    public interface ItemView {
        void bindUser(User user, int position);
    }

    public interface Presenter extends BasePresenter<View> {
        void loadUsers();

        int getUserCount();
        User getUserAt(int position);
        void bindUserListItem(ItemView view, int position);

        void showUserDetail(User user);

        void followUser(User user, int position);
        void blockUser(User user, int position);
        void unFollowUser(User user, int position);
        void unBlockUser(User user, int position);
    }

}
