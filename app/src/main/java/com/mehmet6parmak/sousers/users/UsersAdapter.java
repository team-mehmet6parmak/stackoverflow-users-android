package com.mehmet6parmak.sousers.users;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mehmet6parmak.sousers.databinding.ListItemUserBinding;

import javax.inject.Inject;

/**
 * Created on 6/9/2018.
 */
public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private final UsersPresenter presenter;

    @Inject
    UsersAdapter(UsersPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ListItemUserBinding binding = ListItemUserBinding.inflate(inflater, parent, false);
        return new UserViewHolder(binding, presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        presenter.bindUserListItem(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getUserCount();
    }
}
