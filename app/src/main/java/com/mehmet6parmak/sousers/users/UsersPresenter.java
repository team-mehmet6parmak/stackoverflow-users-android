package com.mehmet6parmak.sousers.users;

import com.mehmet6parmak.sousers.common.BasePresenterImpl;
import com.mehmet6parmak.sousers.common.SchedulerProvider;
import com.mehmet6parmak.sousers.data.UserRepository;
import com.mehmet6parmak.sousers.di.ActivityScope;
import com.mehmet6parmak.sousers.model.User;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created on 6/8/2018.
 */
@ActivityScope
public class UsersPresenter extends BasePresenterImpl<UsersContract.View> implements UsersContract.Presenter {

    private final UserRepository repository;

    @Inject
    public UsersPresenter(SchedulerProvider scheduler, UserRepository repository) {
        super(scheduler);
        this.repository = repository;
    }

    @Override
    public void loadUsers() {
        if (getUserCount() > 0) {
            onUsersLoaded();
        } else {
            loadUserInternal();
        }
    }

    private void loadUserInternal() {
        repository.loadMoreUsers()
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new Observer<List<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposables.add(d);
                        onLoading();
                    }

                    @Override
                    public void onNext(List<User> users) {
                        onUsersLoaded();
                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoadError(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public int getUserCount() {
        return repository.getUsers().size();
    }

    @Override
    public User getUserAt(int position) {
        return repository.getUsers().get(position);
    }

    @Override
    public void bindUserListItem(UsersContract.ItemView view, int position) {
        view.bindUser(getUserAt(position), position);
    }

    @Override
    public void showUserDetail(User user) {
        if (view != null && view.isAlive()) {
            if (!user.isBlocked()) {
                view.onUserDetailRequested(user);
            }
        }
    }

    @Override
    public void followUser(User user, int position) {
        repository.followUser(user)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new UserUpdateObserver(position));
    }

    @Override
    public void blockUser(User user, int position) {
        repository.blockUser(user)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new UserUpdateObserver(position));
    }

    @Override
    public void unFollowUser(User user, int position) {
        repository.unFollowUser(user)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new UserUpdateObserver(position));
    }

    @Override
    public void unBlockUser(User user, int position) {
        repository.unblockUser(user)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(new UserUpdateObserver(position));
    }

    private void onLoading() {
        if (view != null && view.isAlive()) {
            view.showLoading(true);
        }
    }

    private void onUsersLoaded() {
        if (view != null && view.isAlive()) {
            view.onUsersLoaded();
            view.showLoading(false);
        }
    }

    private void onLoadError(Throwable e) {
        if (view != null && view.isAlive()) {
            view.showErrorLayout();
            view.showLoading(false);
        }
    }

    private void onUserUpdated(User user, int position) {
        if (view != null && view.isAlive()) {
            view.onUserUpdated(user, position);
        }
    }

    class UserUpdateObserver implements Observer<User> {

        private final int position;

        public UserUpdateObserver(int position) {
            this.position = position;
        }

        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(User user) {
            onUserUpdated(user, position);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }
}
