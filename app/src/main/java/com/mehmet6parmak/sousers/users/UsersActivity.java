package com.mehmet6parmak.sousers.users;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.mehmet6parmak.sousers.R;
import com.mehmet6parmak.sousers.common.BaseActivity;
import com.mehmet6parmak.sousers.databinding.ActivityUsersBinding;
import com.mehmet6parmak.sousers.model.User;
import com.mehmet6parmak.sousers.userdetail.UserDetailActivity;

import javax.inject.Inject;

public class UsersActivity extends BaseActivity implements UsersContract.View {

    @Inject
    UsersPresenter presenter;

    @Inject
    UsersAdapter adapter;

    private ActivityUsersBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_users);

        component.inject(this);

        setUpUi();

        presenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadUsers();
    }

    private void setUpUi() {
        LinearLayoutManager lm = new LinearLayoutManager(this);
        binding.lstUsers.setLayoutManager(lm);
        binding.lstUsers.setAdapter(adapter);

        binding.btnTryAgain.setOnClickListener(v -> presenter.loadUsers());
    }

    @Override
    public void onUsersLoaded() {
        binding.lstUsers.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorLayout() {
        binding.lstUsers.setVisibility(View.GONE);
        binding.layoutEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading(boolean loading) {
        if (loading) {
            binding.layoutEmpty.setVisibility(View.GONE);
            showLoading();
        }
    }

    @Override
    public void onUserDetailRequested(User user) {
        UserDetailActivity.startWithUser(this, user);
    }

    @Override
    public void onUserUpdated(User user, int userListIndex) {
        adapter.notifyItemChanged(userListIndex);
    }

    @Override
    public boolean isAlive() {
        return isActivityAlive();
    }

    @Override
    protected void onDestroy() {
        presenter.removeView();
        super.onDestroy();
    }
}
