package com.mehmet6parmak.sousers.data.response;

import com.mehmet6parmak.sousers.data.UserStatusService;
import com.mehmet6parmak.sousers.model.Blocked;
import com.mehmet6parmak.sousers.model.Followed;
import com.mehmet6parmak.sousers.model.User;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created on 6/10/2018.
 */
public class LocalUserStatusService implements UserStatusService {

    @Inject
    public LocalUserStatusService() {
    }

    @Override
    public void follow(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> r.insert(new Followed(user.getUserId())));
    }

    @Override
    public void unfollow(User user) {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Followed> rows = realm.where(Followed.class).equalTo(Followed.Contract.COLUMN_USER_ID, user.getUserId())
                .findAll();
        realm.executeTransaction(r -> {
            rows.deleteAllFromRealm();
        });

    }

    @Override
    public void block(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> r.insert(new Blocked(user.getUserId())));
    }

    @Override
    public void unblock(User user) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Blocked> rows = realm.where(Blocked.class).equalTo(Blocked.Contract.COLUMN_USER_ID, user.getUserId()).findAll();

        realm.executeTransaction(r -> rows.deleteAllFromRealm());
    }

    @Override
    public Set<String> getFollowedUsers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Followed> rows = realm.where(Followed.class).findAll();

        Set<String> result = new HashSet<>();
        for (Followed row : rows) {
            result.add(row.getUserId());
        }

        return result;
    }

    @Override
    public Set<String> getBlockedUsers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Blocked> rows = realm.where(Blocked.class).findAll();

        Set<String> result = new HashSet<>();
        for (Blocked row : rows) {
            result.add(row.getUserId());
        }

        return result;
    }

}
