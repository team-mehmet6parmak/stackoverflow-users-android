package com.mehmet6parmak.sousers.data;

import com.mehmet6parmak.sousers.model.User;

import java.util.Set;

/**
 * Created on 6/10/2018.
 */
public interface UserStatusService {

    void follow(User user);
    void unfollow(User user);
    void block(User user);
    void unblock(User user);

    Set<String> getFollowedUsers();
    Set<String> getBlockedUsers();

}
