package com.mehmet6parmak.sousers.data.response;

import com.google.gson.annotations.SerializedName;
import com.mehmet6parmak.sousers.model.User;

import java.util.List;

/**
 * Created on 6/8/2018.
 */
public class UsersResponse {

    @SerializedName("items")
    private List<User> items;

    public List<User> getItems() {
        return items;
    }

    public void setItems(List<User> items) {
        this.items = items;
    }

}
