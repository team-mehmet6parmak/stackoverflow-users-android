package com.mehmet6parmak.sousers.data;




import com.mehmet6parmak.sousers.data.response.UsersResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created on 6/8/2018.
 */
public interface UserService {

    @GET("users")
    Observable<UsersResponse> getUsers(@Query("page") int page, @Query("pagesize") int pageSize, @Query("order") String order, @Query("sort") String sort, @Query("site") String site);

}
