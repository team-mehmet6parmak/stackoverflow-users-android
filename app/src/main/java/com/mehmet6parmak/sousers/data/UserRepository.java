package com.mehmet6parmak.sousers.data;

import android.support.annotation.VisibleForTesting;

import com.mehmet6parmak.sousers.data.response.LocalUserStatusService;
import com.mehmet6parmak.sousers.model.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created on 6/8/2018.
 */
@Singleton
public class UserRepository {

    @VisibleForTesting
    static final int PAGE_SIZE = 20;
    @VisibleForTesting
    static final String ORDER = "desc";
    @VisibleForTesting
    static final String SITE = "stackoverflow";
    @VisibleForTesting
    static final String SORT_ORDER = "reputation";

    private final UserService service;
    private final UserStatusService localUserStatusService;
    private int currentPage = 1;

    private List<User> users = new ArrayList<>();
    private Set<String> usersFollowed = new HashSet<>();
    private Set<String> usersBlocked = new HashSet<>();

    @Inject
    public UserRepository(UserService service, UserStatusService userStatusService) {
        this.service = service;
        this.localUserStatusService = userStatusService;
    }

    public List<User> getUsers() {
        return users;
    }

    public Observable<List<User>> loadMoreUsers() {
        return service.getUsers(currentPage, PAGE_SIZE, ORDER, SORT_ORDER, SITE)
                .map(userResponse -> {
                    checkUserStatusCaches();

                    List<User> list = userResponse.getItems();
                    if (list != null) {
                        //set Followed & Blocked states which does not come from the server.
                        for (User user : list) {
                            user.setFollowed(usersFollowed.contains(user.getUserId()));
                            user.setBlocked(usersBlocked.contains(user.getUserId()));
                        }
                    }

                    users.addAll(list);
                    return users;
                });
    }

    private void checkUserStatusCaches() {
        if (usersFollowed == null || usersFollowed.size() == 0) {
            usersFollowed = localUserStatusService.getFollowedUsers();
        }

        if (usersBlocked == null || usersBlocked.size() == 0) {
            usersBlocked = localUserStatusService.getBlockedUsers();
        }

        //ensure null safety
        if (usersFollowed == null) {
            usersFollowed = new HashSet<>();
        }
        if (usersBlocked == null) {
            usersBlocked = new HashSet<>();
        }
    }

    public Observable<User> getUserDetails(String userId) {
        //simulating a network call
        return Observable.fromCallable(() -> {
            for (User user : users) {
                if (user.getUserId().equals(userId)) {
                    return user;
                }
            }
            throw new Exception("User not found");
        });
    }

    public Observable<User> followUser(User user) {
        return Observable.fromCallable(() -> {
            int userIndex = users.indexOf(user);
            if (userIndex != -1) {
                usersFollowed.add(user.getUserId());
                localUserStatusService.follow(user);
                user.setFollowed(true);
            }
            return user;
        });
    }

    public Observable<User> unFollowUser(User user) {
        return Observable.fromCallable(() -> {
            int userIndex = users.indexOf(user);
            if (userIndex != -1) {
                usersFollowed.remove(user.getUserId());
                localUserStatusService.unfollow(user);
                user.setFollowed(false);
            }
            return user;
        });
    }

    public Observable<User> blockUser(User user) {
        return Observable.fromCallable(() -> {
            int userIndex = users.indexOf(user);
            if (userIndex != -1) {
                usersBlocked.add(user.getUserId());
                localUserStatusService.block(user);
                user.setBlocked(true);
            }
            return user;
        });
    }

    public Observable<User> unblockUser(User user) {
        return Observable.fromCallable(() -> {
            int userIndex = users.indexOf(user);
            if (userIndex != -1) {
                usersBlocked.remove(user.getUserId());
                localUserStatusService.unblock(user);
                user.setBlocked(false);
            }
            return user;
        });
    }
}
