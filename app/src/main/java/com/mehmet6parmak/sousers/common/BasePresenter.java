package com.mehmet6parmak.sousers.common;

/**
 *
 * Base Presenter (MVP's P) interface to be used across the application.
 *
 * Created on 6/8/2018.
 */
public interface BasePresenter<View extends BaseView> {
    void setView(View view);
    void removeView();
}
