package com.mehmet6parmak.sousers.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mehmet6parmak.sousers.SOApplication;
import com.mehmet6parmak.sousers.di.ActivitiesComponent;


/**
 * Created on 6/8/2018.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected ActivitiesComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain component instance on configuration changes.
        if (savedInstanceState != null) {
            component = (ActivitiesComponent) getLastCustomNonConfigurationInstance();
        }

        if (component == null) {
            component = ((SOApplication) getApplication())
                    .getApplicationComponent()
                    .newActivityComponent();
        }
    }

    protected boolean isActivityAlive() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return !isDestroyed();
        }
        return !isFinishing();
    }

    protected void showLoading() {
        Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return component;
    }
}
