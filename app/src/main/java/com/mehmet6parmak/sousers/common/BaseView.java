package com.mehmet6parmak.sousers.common;

/**
 *
 * Base View (MVP's V) interface to be used across the application.
 *
 * Created on 6/8/2018.
 */
public interface BaseView {
    void showLoading(boolean loading);
    boolean isAlive();
}
