package com.mehmet6parmak.sousers.common;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created on 6/9/2018.
 */
public abstract class BasePresenterImpl<ViewClass extends BaseView> implements BasePresenter<ViewClass> {

    protected final CompositeDisposable disposables = new CompositeDisposable();
    protected final SchedulerProvider scheduler;

    protected ViewClass view;

    public BasePresenterImpl(SchedulerProvider scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void setView(ViewClass view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
        this.disposables.clear();
    }
}
