package com.mehmet6parmak.sousers.common;

import io.reactivex.Scheduler;

/**
 *
 * Provides schedulers for different types of tasks.
 *
 * {@link SchedulerProvider#ui()} should be used only for UI related tasks.
 * {@link SchedulerProvider#computation()} should be used for CPU intensive operations.
 * {@link SchedulerProvider#io()} should be used for I/O bound operations, such as network requests.
 *
 * Created on 6/9/2018.
 */
public interface SchedulerProvider {
    Scheduler ui();

    Scheduler computation();

    Scheduler io();
}
