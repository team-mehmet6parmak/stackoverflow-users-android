package com.mehmet6parmak.sousers.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 6/9/2018.
 */
public class DateFormatUtility {

    private static final Locale APP_LOCALE = Locale.UK;
    private static final SimpleDateFormat MEMBER_DATE = new SimpleDateFormat("dd MMM yyyy", APP_LOCALE);

    public static String formatMembershipDate(Long date) {
        return MEMBER_DATE.format(new Date(date * 1000));
    }

}
