package com.mehmet6parmak.sousers.users;

import android.support.annotation.NonNull;

import com.mehmet6parmak.sousers.common.BaseView;
import com.mehmet6parmak.sousers.data.InMemoryUserStatusService;
import com.mehmet6parmak.sousers.data.UserRepository;
import com.mehmet6parmak.sousers.data.UserService;
import com.mehmet6parmak.sousers.model.User;
import com.mehmet6parmak.sousers.utilities.SampleData;
import com.mehmet6parmak.sousers.utilities.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;

import static com.mehmet6parmak.sousers.utilities.SampleData.PAGE_SIZE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created on 6/10/2018.
 */
public class UsersPresenterTest {

    @Mock
    UserService userService;

    @Mock
    UsersContract.View view;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadUsers_shouldCallViewsOnUsersLoaded_whenViewAlive() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();

        verify(view).showLoading(true);
        verify(view).onUsersLoaded();
        verify(view).showLoading(false);
    }

    @Test
    public void loadUsers_shouldCallShowErrorLayout_whenServiceNotAvailable() {
        setError(new Exception());
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();

        verify(view).showLoading(true);
        verify(view).showErrorLayout();
        verify(view).showLoading(false);
    }

    @Test
    public void loadUsers_shouldNotCallViewMethods_whenViewIsNotAttachedEvenOnSuccess() {
        setError(new Exception());
        setViewGone(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();

        verify(view, never()).showErrorLayout();
        verify(view, never()).showLoading(false);
    }

    @Test
    public void loadUsers_shouldNotCallViewMethods_whenViewIsNotAttachedEvenOnError() {
        setData(PAGE_SIZE);
        setViewGone(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();

        verify(view, never()).onUsersLoaded();
        verify(view, never()).showLoading(false);
    }

    @Test
    public void getUserCount_shouldBeEqualToRepositoriesSize() {
        setData(PAGE_SIZE);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.loadUsers();

        assertEquals(presenter.getUserCount(), repository.getUsers().size());
    }

    @Test
    public void getUserAt_shouldReturnTheSameInstanceOnRepository() {
        setData(PAGE_SIZE);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.loadUsers();

        assertEquals(presenter.getUserAt(5), repository.getUsers().get(5));
    }

    @NonNull
    protected UserRepository provideRepository() {
        return new UserRepository(userService, new InMemoryUserStatusService());
    }

    @Test
    public void showUserDetail_shouldCallViewsOnUserDetailRequestedMethod() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);
        presenter.showUserDetail(user);

        verify(view).onUserDetailRequested(user);
    }

    @Test
    public void showUserDetail_shouldNotCallViewsOnUserDetailRequestedMethod_ifUserBlocked() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);

        presenter.blockUser(user, 0);
        presenter.showUserDetail(user);

        verify(view, never()).onUserDetailRequested(user);
    }

    @Test
    public void followUser_shouldCallViewsOnUserUpdatedMethod_whenViewAlive() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);
        presenter.followUser(user, 0);

        verify(view).onUserUpdated(user, 0);
        assertTrue(user.isFollowed());
    }

    @Test
    public void unfollowUser_shouldCallViewsOnUserUpdatedMethodAndSetUserUnfollowed() {
        setFollowedData();
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);

        presenter.unFollowUser(user, 0);

        verify(view).onUserUpdated(user, 0);
        assertFalse(user.isFollowed());
    }

    @Test
    public void blockUser_shouldCallViewsOnUserUpdatedMethodAndSetUserBlocked() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);

        presenter.blockUser(user, 0);

        verify(view).onUserUpdated(user, 0);
        assertTrue(user.isBlocked());
    }

    @Test
    public void unblockUser_shouldCallViewsOnUserUpdatedMethodAndSetUserUnblocked() {
        setBlocked();
        setViewAlive(view);

        UserRepository repository = provideRepository();
        UsersPresenter presenter = new UsersPresenter(new TestSchedulerProvider(), repository);

        presenter.setView(view);
        presenter.loadUsers();
        User user = presenter.getUserAt(0);

        presenter.unBlockUser(user, 0);

        verify(view).onUserUpdated(user, 0);
        assertFalse(user.isBlocked());
    }

    private void setViewAlive(BaseView view) {
        when(view.isAlive()).thenReturn(true);
    }

    private void setViewGone(BaseView view) {
        when(view.isAlive()).thenReturn(false);
    }

    private void setBlocked() {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeBlockedUsers(PAGE_SIZE));
    }

    private void setFollowedData() {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeFollowedUsers(PAGE_SIZE));
    }

    private void setData(int numberOfUsers) {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeUsers(numberOfUsers));
    }

    private void setError(Throwable e) {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(Observable.error(e));
    }
}