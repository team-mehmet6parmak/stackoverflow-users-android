package com.mehmet6parmak.sousers.utilities;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created on 6/10/2018.
 */
public class DateFormatUtilityTest {

    @Test
    public void formatMembershipDate_shouldStillReturnEnglish_whenUserLocaleDifferent() {
        Locale.setDefault(new Locale("tr", "TR"));

        String result = DateFormatUtility.formatMembershipDate(1222430705L);

        assertEquals(result, "26 Sep 2008");
    }
}