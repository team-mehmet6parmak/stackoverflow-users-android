package com.mehmet6parmak.sousers.utilities;

import com.mehmet6parmak.sousers.data.response.UsersResponse;
import com.mehmet6parmak.sousers.model.User;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 6/10/2018.
 */
public class SampleData {

    public static final int PAGE_SIZE = 20;

    public static Observable<UsersResponse> getSomeUsers(int numberOfUsers) {
        List<User> users = new ArrayList<>();

        for (int i = 0; i < numberOfUsers; i++) {

            String userId = String.valueOf(i + 1);

            User user = new User();
            user.setUserId(userId);
            user.setName("User " + userId);
            user.setProfileImage("http://google.com/" + userId);
            user.setReputation(Long.parseLong(userId));

            users.add(user);
        }

        UsersResponse response = new UsersResponse();
        response.setItems(users);

        return Observable.just(response);
    }

    public static Observable<UsersResponse> getSomeFollowedUsers(int numberOfUsers) {
        return getSomeUsers(numberOfUsers).map(response -> {

            for (User user : response.getItems()) {
                user.setFollowed(true);
            }

            return response;
        });
    }

    public static Observable<UsersResponse> getSomeBlockedUsers(int numberOfUsers) {
        return getSomeUsers(numberOfUsers).map(response -> {

            for (User user : response.getItems()) {
                user.setBlocked(true);
            }

            return response;
        });
    }
}
