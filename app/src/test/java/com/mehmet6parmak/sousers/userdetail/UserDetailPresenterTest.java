package com.mehmet6parmak.sousers.userdetail;

import android.support.annotation.NonNull;

import com.mehmet6parmak.sousers.common.BaseView;
import com.mehmet6parmak.sousers.data.InMemoryUserStatusService;
import com.mehmet6parmak.sousers.data.UserRepository;
import com.mehmet6parmak.sousers.data.UserService;
import com.mehmet6parmak.sousers.model.User;
import com.mehmet6parmak.sousers.utilities.SampleData;
import com.mehmet6parmak.sousers.utilities.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.mehmet6parmak.sousers.utilities.SampleData.PAGE_SIZE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created on 6/10/2018.
 */
public class UserDetailPresenterTest {

    @Mock
    UserService userService;

    @Mock
    UserDetailContract.View view;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadUserDetail_shouldCallOnUserDetailLoaded_whenNothingExtraOrdinaryHappened() {
        User userNumberFive = loadUserDetailWith("5", true);

        verify(view).showLoading(true);
        verify(view).onUserDetailLoaded(userNumberFive);
        verify(view).showLoading(false);
    }

    @Test
    public void loadUserDetail_shouldCallErrorCallback_whenAnInvalidUserIdGiven() {
        loadUserDetailWith("21", true);

        verify(view).showLoading(true);
        verify(view).showLoading(false);
        verify(view).onError();
    }

    @Test
    public void loadUserDetail_shouldNotCallOnUserDetailLoaded_whenViewIsNotAttached() {
        User userNumberFive = loadUserDetailWith("5", false);

        verify(view, never()).onUserDetailLoaded(userNumberFive);
        verify(view, never()).showLoading(false);
    }

    @Test
    public void followUser_shouldCallViewsOnUserUpdatedMethod_whenViewAlive() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        repository.loadMoreUsers().subscribe();

        UserDetailPresenter presenter = new UserDetailPresenter(repository, new TestSchedulerProvider());

        presenter.setView(view);
        User user = repository.getUsers().get(0);
        presenter.followUser(user);

        verify(view).onUserDetailLoaded(user);
        assertTrue(user.isFollowed());
    }

    @Test
    public void unfollowUser_shouldCallViewsOnUserUpdatedMethodAndSetUserUnfollowed() {
        setFollowedData();
        setViewAlive(view);

        UserRepository repository = provideRepository();
        repository.loadMoreUsers().subscribe();

        UserDetailPresenter presenter = new UserDetailPresenter(repository, new TestSchedulerProvider());

        presenter.setView(view);
        User user = repository.getUsers().get(0);
        presenter.unFollowUser(user);

        verify(view).onUserDetailLoaded(user);
        assertFalse(user.isFollowed());
    }

    @Test
    public void blockUser_shouldCallViewsOnUserUpdatedMethodAndSetUserBlocked() {
        setData(PAGE_SIZE);
        setViewAlive(view);

        UserRepository repository = provideRepository();
        repository.loadMoreUsers().subscribe();

        UserDetailPresenter presenter = new UserDetailPresenter(repository, new TestSchedulerProvider());

        presenter.setView(view);

        User user = repository.getUsers().get(0);
        presenter.blockUser(user);

        verify(view).onUserDetailLoaded(user);
        assertTrue(user.isBlocked());
    }

    @Test
    public void unblockUser_shouldCallViewsOnUserUpdatedMethodAndSetUserUnblocked() {
        setBlocked();
        setViewAlive(view);

        UserRepository repository = provideRepository();
        repository.loadMoreUsers().subscribe();

        UserDetailPresenter presenter = new UserDetailPresenter(repository, new TestSchedulerProvider());

        presenter.setView(view);

        User user = repository.getUsers().get(0);
        presenter.unBlockUser(user);

        verify(view).onUserDetailLoaded(user);
        assertFalse(user.isBlocked());
    }

    private User loadUserDetailWith(String userId, boolean viewAlive) {
        setData(PAGE_SIZE);
        if (viewAlive)
            setViewAlive(view);
        else
            setViewGone(view);

        UserRepository repository = provideRepository();
        UserDetailPresenter presenter = new UserDetailPresenter(repository, new TestSchedulerProvider());
        presenter.setView(view);

        repository.loadMoreUsers().subscribe();
        User userNumberFive = repository.getUsers().get(4);

        presenter.loadUserDetail(userId);
        return userNumberFive;
    }

    @NonNull
    private UserRepository provideRepository() {
        return new UserRepository(userService, new InMemoryUserStatusService());
    }

    private void setViewAlive(BaseView view) {
        when(view.isAlive()).thenReturn(true);
    }

    private void setViewGone(BaseView view) {
        when(view.isAlive()).thenReturn(false);
    }

    private void setBlocked() {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeBlockedUsers(PAGE_SIZE));
    }

    private void setFollowedData() {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeFollowedUsers(PAGE_SIZE));
    }


    private void setData(int numberOfUsers) {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeUsers(numberOfUsers));
    }

}