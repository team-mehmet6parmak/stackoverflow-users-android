package com.mehmet6parmak.sousers.data;

import android.support.annotation.NonNull;

import com.mehmet6parmak.sousers.model.User;
import com.mehmet6parmak.sousers.utilities.SampleData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static com.mehmet6parmak.sousers.data.UserRepository.ORDER;
import static com.mehmet6parmak.sousers.data.UserRepository.PAGE_SIZE;
import static com.mehmet6parmak.sousers.data.UserRepository.SITE;
import static com.mehmet6parmak.sousers.data.UserRepository.SORT_ORDER;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created on 6/10/2018.
 */
public class UserRepositoryTest {

    @Mock
    UserService userService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUsers_shouldReturnUserList_whenServiceAvailable() {
        setData(PAGE_SIZE);

        UserRepository repository = provideRepository();

        TestObserver<List<User>> observer = new TestObserver<>();
        repository.loadMoreUsers().subscribe(observer);

        verify(userService).getUsers(1, PAGE_SIZE, ORDER, SORT_ORDER, SITE);
        assertEquals(observer.valueCount(), 1);
        assertEquals(observer.values().get(0).size(), PAGE_SIZE);
        assertEquals(repository.getUsers().size(), PAGE_SIZE);
    }

    @Test
    public void getUsers_shouldReturnError_whenServiceNotAvailable() {
        Exception error = new Exception();
        setError(error);

        UserRepository repository = provideRepository();

        TestObserver<List<User>> observer = new TestObserver<>();
        repository.loadMoreUsers().subscribe(observer);

        verify(userService).getUsers(1, PAGE_SIZE, ORDER, SORT_ORDER, SITE);
        observer.assertError(error);
        assertEquals(repository.getUsers().size(), 0);
    }

    @Test
    public void getUserDetail_shouldReturnUserObjectWithGivenUserId_whenUserIdIsValid() {
        setData(PAGE_SIZE);

        UserRepository repository = provideRepository();

        repository.loadMoreUsers().subscribe();
        User userNumberSix = repository.getUsers().get(5);

        TestObserver<User> observer = new TestObserver<>();
        repository.getUserDetails(userNumberSix.getUserId()).subscribe(observer);

        assertEquals(observer.valueCount(), 1);
        assertEquals(userNumberSix, observer.values().get(0));
    }

    @Test
    public void getUserDetail_shouldReturnError_whenUserIdIsInValid() {
        setData(PAGE_SIZE);

        UserRepository repository = provideRepository();

        repository.loadMoreUsers().subscribe();

        TestObserver<User> observer = new TestObserver<>();
        repository.getUserDetails("21").subscribe(observer);

        assertTrue(observer.errorCount() > 0);
        assertEquals(observer.errors().get(0).getMessage(), "User not found");
    }

    @NonNull
    protected UserRepository provideRepository() {
        return new UserRepository(userService, new InMemoryUserStatusService());
    }

    private void setData(int numberOfUsers) {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(SampleData.getSomeUsers(numberOfUsers));
    }

    private void setError(Throwable e) {
        when(userService.getUsers(eq(1), eq(PAGE_SIZE), anyString(), anyString(), anyString()))
                .thenReturn(Observable.error(e));
    }
}