package com.mehmet6parmak.sousers.data;

import com.mehmet6parmak.sousers.model.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created on 6/10/2018.
 */
public class InMemoryUserStatusService implements UserStatusService {

    Set<String> followedUsers = new HashSet<>();
    Set<String> blockedUsers = new HashSet<>();

    @Override
    public void follow(User user) {
        followedUsers.add(user.getUserId());
    }

    @Override
    public void unfollow(User user) {
        followedUsers.remove(user.getUserId());
    }

    @Override
    public void block(User user) {
        blockedUsers.add(user.getUserId());
    }

    @Override
    public void unblock(User user) {
        blockedUsers.remove(user.getUserId());
    }

    @Override
    public Set<String> getFollowedUsers() {
        return new HashSet<>(followedUsers);
    }

    @Override
    public Set<String> getBlockedUsers() {
        return new HashSet<>(blockedUsers);
    }
}
